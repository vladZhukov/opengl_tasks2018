#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>

/**
Несколько примеров шейдеров
*/
class SampleApplication : public Application
{
public:
    MeshPtr _moebius_strip;
    MeshPtr _bunny;

    ShaderProgramPtr _shader;

    GLuint _ubo;

    GLuint uniformBlockBinding = 0;
    size_t resolution = 50;
    size_t resolution_step = 5;
    bool minus_released = true;
    bool plus_released = true;
    float mobius_radius = 1.2;
    float mobius_inside_radius = 2;

    void makeScene() override
    {
        Application::makeScene();

        _moebius_strip = makeMoebiusStrip(mobius_radius, mobius_inside_radius, resolution);
        _moebius_strip->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        _bunny = loadFromFile("499ZhukovData/models/bunny.obj");
        _bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("499ZhukovData/moebius.vert", "499ZhukovData/shader.frag");

        //=========================================================
        //Инициализация Uniform Buffer Object

        // Выведем размер Uniform block'а.
        GLint uniformBlockDataSize;
        glGetActiveUniformBlockiv(_shader->id(), 0, GL_UNIFORM_BLOCK_DATA_SIZE, &uniformBlockDataSize);
        std::cout << "Uniform block 0 data size = " << uniformBlockDataSize << std::endl;

        if (USE_DSA) {
            glCreateBuffers(1, &_ubo);
            glNamedBufferData(_ubo, uniformBlockDataSize, nullptr, GL_DYNAMIC_DRAW);
        }
        else {
            glGenBuffers(1, &_ubo);
            glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
            glBufferData(GL_UNIFORM_BUFFER, uniformBlockDataSize, nullptr, GL_DYNAMIC_DRAW);
            glBindBuffer(GL_UNIFORM_BUFFER, 0);
        }
        // Привязываем буффер к точке привязки Uniform буферов.
        glBindBufferBase(GL_UNIFORM_BUFFER, uniformBlockBinding, _ubo);


        // Получение информации обо всех uniform-переменных шейдерной программы.
        if (USE_INTERFACE_QUERY) {
            GLsizei uniformsCount;
            GLsizei maxNameLength;
            glGetProgramInterfaceiv(_shader->id(), GL_UNIFORM, GL_ACTIVE_RESOURCES, &uniformsCount);
            glGetProgramInterfaceiv(_shader->id(), GL_UNIFORM, GL_MAX_NAME_LENGTH, &maxNameLength);
            std::vector<char> nameBuffer(maxNameLength);

            std::vector<GLenum> properties = {GL_TYPE, GL_ARRAY_SIZE, GL_OFFSET, GL_BLOCK_INDEX};
            enum Property {
                Type,
                ArraySize,
                Offset,
                BlockIndex
            };
            for (GLuint uniformIndex = 0; uniformIndex < uniformsCount; uniformIndex++) {
                std::vector<GLint> params(properties.size());
                glGetProgramResourceiv(_shader->id(), GL_UNIFORM, uniformIndex, properties.size(), properties.data(),
                                       params.size(), nullptr, params.data());
                GLsizei realNameLength;
                glGetProgramResourceName(_shader->id(), GL_UNIFORM, uniformIndex, maxNameLength, &realNameLength, nameBuffer.data());

                std::string uniformName = std::string(nameBuffer.data(), realNameLength);

                std::cout << "Uniform " << "index = " << uniformIndex << ", name = " << uniformName << ", block = " << params[BlockIndex] << ", offset = " << params[Offset] << ", array size = " << params[ArraySize] << ", type = " << params[Type] << std::endl;
            }
        }
        else {
            GLsizei uniformsCount;
            glGetProgramiv(_shader->id(), GL_ACTIVE_UNIFORMS, &uniformsCount);

            std::vector<GLuint> uniformIndices(uniformsCount);
            for (int i = 0; i < uniformsCount; i++)
                uniformIndices[i] = i;
            std::vector<GLint> uniformBlocks(uniformsCount);
            std::vector<GLint> uniformNameLengths(uniformsCount);
            std::vector<GLint> uniformTypes(uniformsCount);
            glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_BLOCK_INDEX, uniformBlocks.data());
            glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_NAME_LENGTH, uniformNameLengths.data());
            glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_TYPE, uniformTypes.data());

            for (int i = 0; i < uniformsCount; i++) {
                std::vector<char> name(uniformNameLengths[i]);
                GLsizei writtenLength;
                glGetActiveUniformName(_shader->id(), uniformIndices[i], name.size(), &writtenLength, name.data());
                std::string uniformName = name.data();

                std::cout << "Uniform " << "index = " << uniformIndices[i] << ", name = " << uniformName << ", block = " << uniformBlocks[i] << ", type = " << uniformTypes[i] << std::endl;
            }
        }
    }

    void update() override
    {
        Application::update();

        //Обновляем содержимое Uniform Buffer Object

#if 0
        //Вариант с glMapBuffer
        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
        GLvoid* p = glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
        memcpy(p, &_camera, sizeof(_camera));
        glUnmapBuffer(GL_UNIFORM_BUFFER);
#elif 0
        //Вариант с glBufferSubData
        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(_camera), &_camera);
#else
        //Вариант для буферов, у которых layout отличается от std140

        //Имена юниформ-переменных
        const char* names[2] =
                {
                        "viewMatrix",
                        "projectionMatrix"
                };

        GLuint index[2];
        GLint offset[2];

        //Запрашиваем индексы 2х юниформ-переменных
        glGetUniformIndices(_shader->id(), 2, names, index);

        //Зная индексы, запрашиваем сдвиги для 2х юниформ-переменных
        glGetActiveUniformsiv(_shader->id(), 2, index, GL_UNIFORM_OFFSET, offset);

        // Вывод оффсетов.
        static bool hasOutputOffset = false;
        if (!hasOutputOffset) {
            std::cout << "Offsets: viewMatrix " << offset[0] << ", projMatrix " << offset[1] << std::endl;
            hasOutputOffset = true;
        }

        //Устанавливаем значения 2х юниформ-перменных по отдельности
        if (USE_DSA) {
            glNamedBufferSubData(_ubo, offset[0], sizeof(_camera.viewMatrix), &_camera.viewMatrix);
            glNamedBufferSubData(_ubo, offset[1], sizeof(_camera.projMatrix), &_camera.projMatrix);
        }
        else {
            glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
            glBufferSubData(GL_UNIFORM_BUFFER, offset[0], sizeof(_camera.viewMatrix), &_camera.viewMatrix);
            glBufferSubData(GL_UNIFORM_BUFFER, offset[1], sizeof(_camera.projMatrix), &_camera.projMatrix);
        }
#endif
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        unsigned int blockIndex = glGetUniformBlockIndex(_shader->id(), "Matrices");
        glUniformBlockBinding(_shader->id(), blockIndex, uniformBlockBinding);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _moebius_strip->modelMatrix());


        _moebius_strip->draw();
        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_RELEASE && !minus_released) {
            minus_released = true;
        }
        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS && minus_released) {
            minus_released = false;
            resolution = std::max<size_t >(resolution - resolution_step, resolution_step);
            _moebius_strip = makeMoebiusStrip(mobius_radius, mobius_inside_radius, resolution);
            _moebius_strip->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        }
        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_RELEASE && !plus_released) {
            plus_released= true;
        }
        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS && plus_released) {
            plus_released = false;
            resolution = std::max<size_t >(resolution + resolution_step, resolution_step);
            _moebius_strip = makeMoebiusStrip(mobius_radius, mobius_inside_radius, resolution);
            _moebius_strip->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        }

        _shader->setMat4Uniform("modelMatrix", _bunny->modelMatrix());
        _bunny->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}