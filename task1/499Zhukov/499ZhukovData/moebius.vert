#version 330

layout(std140) uniform Matrices
{
    mat4 viewMatrix;
    mat4 projectionMatrix;
};

uniform mat4 modelMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec4 normal;

out vec4 color;

void main()
{
    color = normal * 0.5 + 0.5;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
